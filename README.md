# About This Tool

This is a basic tool that can be used to work with the `message_all.dat` file used by **Metroid: Other M** to provide translation text. While not all of the text is subtitles, this tool was created to inject a fan translation of the subtitles into the game.

## Build Instructions

Download the latest .NET 5.0 SDK for your operating system from [this page](https://dotnet.microsoft.com/download/dotnet/5.0).

After cloning this repository, publish and run the app by following the instructions in [this article](https://docs.microsoft.com/en-us/dotnet/core/tutorials/publishing-with-visual-studio-code).

## Basic Usage

Convert `message_all.dat` to a CSV file.
```
OtherMSubtitles.exe --type=csv message_all.dat message_all.csv
```

Convert a CSV file to a `message_all.dat` file ready to inject in-game.
```
OtherMSubtitles.exe new_subtitles.csv message_all.dat
```

See the help command for more info.
```
OtherMSubtitles.exe --help
```

## Enable Japanese Voiceovers

You can enable the Japanese version of the game with the following gecko code.

```
04003180 52334f4a
```
