# Information About the message_all.dat File

This is a detailed overview of some of the discoveries made about the `message_all.dat` file. It isn't necessary to understand this in order to use the `OtherMSubtitles` tool.


## Conventions Used

For the purpose of this document, one word will be considered 4-bytes, or 32-bits, in length.

Offsets will be given in hexadecimal, and values should be given in decimal unless stated otherwise. Where it is not obvious, hexadecimal values will be prefixed with the **0x** character sequence.

## File Header

The file header is in big endian order.

The first six bytes use the following magic number to identify the file. This number is null-padded to fill up the first two words.

| Hexadecimal       | ASCII  |
| ----------------- | ------ |
| 74 64 70 61 68 6B | tdpack |

The next four bytes contain some settings.

| Byte Offset | Description                                                            |
| ----------- | ---------------------------------------------------------------------- |
| 8           | Must be 255. I believe this to be the requested message buffer length. |
| 9           | Not used.                                                              |
| A           | Unknown flag. Can either be 1 or 0. This was set to 1 on release.      |
| B           | Unknown flag. Can either be 1 or 0. This was cleared to 0 on release.  |

---
**NOTE**

Either A or B must be 1. The game is known to crash if both are cleared to 0.

---

The following words complete the first part of the file header.

| Word Offset | Description                              | Initial Value |
| ----------- | ---------------------------------------- | ------------: |
| 0C          | The length of this section in bytes.     |            48 |
| 10          | The length of the file in bytes.         |       730,752 |
| 14          | The number of offsets in the file.       |             8 |
| 18          | The number of lengths in the file.       |             8 |
| 1C          | Not used.                                |             0 |
| 20          | The offset where the text offsets start. |            48 |
| 24          | The offset where the text lengths begin. |            80 |
| 28          | Not used.                                |             0 |
| 2C          | Not used.                                |             0 |

The next set of words describe the structure of the translation text. These words are essentially two arrays which indicate the offsets and lengths, respectively, of each section. Each translation section will have an offset and length pair associated with it. The first array, starting at 0x30, points to each offset in  the file. The second array, starting at 0x50, indicates the length of each section.

Finally, bytes 70 through 7F are all null, thus completing the file header.

Overall, I think this was some sort of generic file format that was repurposed to store the translation text. One way to look at this file is as a series of files concatenated together. I'd be curious to know if somebody else has this file format documented when it just happened to be used for something else.

## Translation Text

The following table lists the initial offsets, lengths, line numbers, and language IDs of all of the translation text in this file. Every line following each offset contains the language identifier.

| Offset | Length   | Line Number | Language ID |
| ------ | -------: | ----------: | ----------: |
| 00080  |  104,245 |           3 |          JP |
| 197C0  |   82,849 |       1,902 |          US |
| 2DB80  |   91,414 |       3,801 |          DE |
| 440A0  |   96,964 |       5,700 |          FR |
| 5BB80  |   86,573 |       7,600 |          ES |
| 70DC0  |   87,708 |       9,499 |          IT |
| 86460  |   95,719 |      11,398 |        USFR |
| 9DA60  |   85,022 |      13,297 |        USES |

Each section is null-padded to ensure that each section aligns to a 32-byte boundary.

Each line is surrounded by quotes where the backslash (\, 92, 0x5C) is used as the quote character.

Although CRLF is used as the line ending in the file, the game only looks for the linefeed characters when counting lines.

The FR section is reported as 1,900 lines in length in some text editors due to an error at 0x4FE5A. Replacing the carriage return (13, 0xD) character at this position with a space (64, 0x20) character is enough to resolve this issue without breaking the file.

Lines are specified using the caret (^) character.

As backslashes, carets, and linefeeds are special characters, none of these characters are allowed to show up in the translation texts.

## Mission Summaries

The mission summaries, which can be viewed after saving the game and restarting, sometimes have extra options. These extra options come in the form of resources which are briefly described using the following table. Resources are specified using the hash (#) symbol. Each line can optionally specify a font (although **#FONT_SYSTEM** is the only one allowed if specified), color, icon, or image as resources.

| Prefix       | Description                                                |
| ------------ | ---------------------------------------------------------- |
| #FONT_SYSTEM | Used to activate the use of the system font.               |
| #COLOR_      | The color to use for the following text.                   |
| #ICON_       | The icon (controller button) to show inline with the text. |
| #IMG_        | The image to display at this point of the text.            |
