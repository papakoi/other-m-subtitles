﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using Mono.Options;

namespace OtherMSubtitles
{
    class Program
    {
        static void Main(string[] args)
        {
            MyOptions options = null;
            try
            {
                options = new MyOptions(args);
            }
            catch (Exception ex)
            {
                Console.Write ("OtherMSubtitles.exe: ");
                Console.WriteLine (ex.Message);
                Console.WriteLine ("Try `OtherMSubtitles.exe --help' for more information.");
                return;
            }

            if (options.ShouldShowHelp)
            {
                options.ShowHelp();
                return;
            }

            if (options.OutputType == OutputType.Tdpack)
            {
                using var inputFile = new StreamReader(options.InputFilename);
                using var csv = new CsvReader(inputFile, CultureInfo.InvariantCulture);
                csv.Read();
                csv.ReadHeader();
                
                // Assuming a set of columns, one for each language,
                // plus an additional column indicating the line number.

                var subtitles = new List<string>[csv.HeaderRecord.Length - 1];
                for (int i = 0; i != subtitles.Length; ++i)
                {
                    subtitles[i] = new List<string>();
                }

                while (csv.Read())
                {
                    for (int i = 0; i != subtitles.Length; ++i)
                    {
                        // Assume the first column is the line number.
                        subtitles[i].Add(csv.GetField(i + 1));
                    }
                }

                using FileStream outputFile = File.OpenWrite(options.OutputFilename);
                TdpackHelper.WriteTdpackFile(outputFile, subtitles);
                return;
            }

            if (options.OutputType == OutputType.Csv)
            {
                using FileStream inputFile = File.OpenRead(options.InputFilename);
                using var outputFile = new StreamWriter(options.OutputFilename);
                using var csv = new CsvWriter(outputFile, CultureInfo.InvariantCulture);
                csv.WriteRecords(TdpackHelper.ReadSubtitles(inputFile));
                return;
            }

            throw new InvalidOperationException("OutputType." + options.OutputType + " not supported.");
        }

        public class MyOptions
        {
            public MyOptions(string[] args)
            {
                List<string> extras = OptionSet.Parse(args);

                if (ShouldShowHelp)
                {
                    return;
                }

                if (extras.Count == 0)
                {
                    throw new Exception("Must provide an input filename.");
                }

                InputFilename = extras[0];

                if (extras.Count > 1)
                {
                    OutputFilename = extras[1];
                    return;
                }

                OutputFilename = OutputType == OutputType.Tdpack ? "out.dat" : "out.csv";
            }

            public void ShowHelp()
            {
                Console.WriteLine("Usage: OtherMSubtitles.exe [option] input-file [output-file]");
                Console.WriteLine("Converts the input subtitles file to the desired format. If the output filename");
                Console.WriteLine("is not specified, then either \"out.dat\" or \"out.csv\" will be used.");
                Console.WriteLine();
                Console.WriteLine("Options:");
                OptionSet.WriteOptionDescriptions(Console.Out);
                Console.WriteLine();
                Console.WriteLine("Examples:");
                Console.WriteLine("OtherMSubtitles.exe new-subtitles.csv");
                Console.WriteLine("    Creates a new TDPACK file.");
                Console.WriteLine();
                Console.WriteLine("OtherMSubtitles.exe --type=tdpack new-subtitles.csv out.dat");
                Console.WriteLine("    Same as above, but explicit.");
                Console.WriteLine();
                Console.WriteLine("OtherMSubtitles.exe --type=csv message_all.dat original.csv");
                Console.WriteLine("    Converts the TDPACK file to CSV.");
            }
            
            public OptionSet OptionSet
            { 
                get => new OptionSet
                { 
                    {
                        "h|help",
                        "show this message and exit",
                        h => ShouldShowHelp = h is not null
                    },
                    { 
                        "t|type=",
                        "the type of conversion to make; either tdpack or csv; defaults to tdpack",
                        t =>
                        {
                            if (t is null)
                            {
                                OutputType = OutputType.Tdpack;
                            }
                            else if (t == "tdpack")
                            {
                                OutputType = OutputType.Tdpack;
                            }
                            else if (t == "csv")
                            {
                                OutputType = OutputType.Csv;
                            }
                            else
                            {
                                throw new Exception("Unsupported conversion type. Value: " + t);
                            }
                        }
                    },
                };
            }
            public bool ShouldShowHelp { get; set; }
            public string InputFilename { get; set; } 
            public string OutputFilename { get; set; }
            public OutputType OutputType { get; set; }

        }

        public enum OutputType
        {
            Tdpack, Csv
        }
    }
}
