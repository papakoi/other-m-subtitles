using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Kermalis.EndianBinaryIO;

namespace OtherMSubtitles
{
    public static class TdpackHelper
    {
        /// <summary>
        /// Grab each subtitle for the requested language. 
        /// </summary>
        public static IEnumerable<string> ReadSubtitles(Stream inputFile, Language language)
        {
            // Grabbing the file metadata makes the requested
            // operation more efficient.

            var binaryReader = new EndianBinaryReader
            (
                inputFile,
                Endianness.BigEndian
            );

            inputFile.Position = 32;
            int offsetsLocation = binaryReader.ReadInt32();
            int lengthsLocation = binaryReader.ReadInt32();

            // There are only 8 languages whose offsets and lengths
            // exist as two arrays towards the beginning of the file.
            // It's safe to assume that the position of the reqested
            // language in the enum should match the requested index.

            int requestedIndex = (int)language * sizeof(int);
            int offset = binaryReader.ReadInt32(offsetsLocation + requestedIndex);
            int length = binaryReader.ReadInt32(lengthsLocation + requestedIndex);

            // Loading the requested subtitles into memory helps to
            // mitigate any weirdness that might crop up by reading
            // beyond the byte length of the section.

            var buffer = new byte[length];
            inputFile.Position = offset;
            inputFile.Read(buffer);

            Stream memory = new MemoryStream(buffer);
            memory.Position = 0;

            var textReader = new StreamReader
            (
                memory,
                Encoding.UTF8,
                detectEncodingFromByteOrderMarks: false,
                leaveOpen: false
            );

            if (textReader.EndOfStream)
            {
                throw new InvalidOperationException("Unexpected end of stream.");
            }

            while (!textReader.EndOfStream)
            {
                string text = textReader.ReadLine();
                if (text is null)
                {
                    break;
                }

                // This is to get around the error of the lone
                // carriage return being in the original file.

                while (!text.EndsWith('\\'))
                {
                    string nextLine = textReader.ReadLine();
                    if (nextLine is null)
                    {
                        break;
                    }
                    text += " " + nextLine;
                }

                yield return text
                    .Trim('\\')
                    .Replace("^", Environment.NewLine);
            }
        }

        /// <summary>
        /// Grab the subtitles for every language.
        /// </summary>
        /// <returns>
        /// An enumerable where each item has the line number of the
        /// subtitle and all the languages for that line.
        /// </returns>
        public static IEnumerable<object> ReadSubtitles(Stream inputFile)
        {
            var subtitles = new Dictionary<string, IEnumerator<string>>();
            foreach (Language language in Enum.GetValues<Language>())
            {
                subtitles.Add(language.ToString(), ReadSubtitles(inputFile, language).GetEnumerator());
            }
            var hasValue = true;
            var lineNumber = 0;
            while (hasValue)
            {
                var item = new ExpandoObject();
                item.TryAdd("LineNumber", lineNumber++);
                hasValue = false;
                foreach (KeyValuePair<string, IEnumerator<string>> section in subtitles)
                {
                    bool sectionEmpty = !section.Value.MoveNext();
                    if (sectionEmpty)
                    {
                        continue;
                    }
                    hasValue = true;
                    item.TryAdd(section.Key, section.Value.Current);
                }
                if (hasValue)
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Create a TDPACK file using the given sets of subtitles. 
        /// </summary>
        public static void WriteTdpackFile(Stream stream, params IEnumerable<string>[] subtitleSections)
        {
            // Prepare to write the header last as the byte lengths and
            // offsets of each subtitle section are unknown.

            int metadataLength = 48;
            int wordSize = sizeof(int);
            int headerLength = metadataLength + subtitleSections.Length * wordSize;

            stream.Position = headerLength + PaddingBytesLength(headerLength, 64);

            // After writing each subtitle section in the file, make a note
            // of the byte offset and length so that this information can 
            // later be used to write the header.

            int count = subtitleSections.Length;
            var offsets = new int[count];
            var lengths = new int[count];
            var index = 0;
            foreach (IEnumerable<string> section in subtitleSections)
            {
                stream.Position += PaddingBytesLength(stream.Position, byteAlignment: 32);
                offsets[index] = (int)stream.Position;
                WriteSubtitles(stream, section);
                lengths[index] = (int)stream.Position - offsets[index];
                stream.Position += PaddingBytesLength(stream.Position, byteAlignment: 16);
                ++index;
            }

            // Truncate any unused bytes from the file.

            int fileLength = (int)stream.Position;
            stream.SetLength(fileLength);

            // The header can now be written since we've finished
            // writing the rest of the file.

            stream.Position = 0;

            var writer = new EndianBinaryWriter
            (
                stream,
                Endianness.BigEndian,
                EncodingType.ASCII,
                BooleanSize.U8
            );

            string magicNumber = "tdpack";
            writer.Write(magicNumber, nullTerminated: false);

            stream.Position = wordSize * 2;
            byte messageBufferLength = 255;
            writer.Write(messageBufferLength);

            stream.Position += 1;
            var flag1 = true;
            writer.Write(flag1);
            var flag2 = false;
            writer.Write(flag2);

            writer.Write(metadataLength);
            writer.Write(fileLength);
            writer.Write(offsets.Length);
            writer.Write(lengths.Length);

            stream.Position += wordSize;
            int offsetsStart = metadataLength;
            writer.Write(offsetsStart);

            int lengthsStart = offsetsStart + offsets.Length * wordSize;
            lengthsStart += PaddingBytesLength(lengthsStart, byteAlignment: 16);
            writer.Write(lengthsStart);

            stream.Position = offsetsStart;
            writer.Write(offsets);

            stream.Position = lengthsStart;
            writer.Write(lengths);

            // Need to call flush or data may not be written to file.

            stream.Flush();
        }

        /// <summary>
        /// Get the number of bytes needed to align the given length to the specified byte alignment.
        /// </summary>
        public static int PaddingBytesLength(long length, int byteAlignment)
        {
            if (byteAlignment <= 0)
            {
                throw new ArgumentOutOfRangeException("Alignment must be a positive, non-zero integer.", nameof(byteAlignment));
            }
            int lessOne = byteAlignment - 1;
            int check = byteAlignment & lessOne;
            if (check != 0)
            {
                throw new ArgumentException("Alignment must be a power of 2.", nameof(byteAlignment));
            }
            return (int)(byteAlignment - length & lessOne);
        }

        /// <summary>
        /// Write the list of subtitles to the given stream.
        /// </summary>
        public static void WriteSubtitles(Stream stream, IEnumerable<string> translatableText)
        {
            foreach (string text in translatableText)
            {
                byte[] bytes = GetSubtitleBytes(text);
                stream.Write(bytes);
            }
        }

        /// <summary>
        /// Convert the given input to subtitle bytes. During this process, any
        /// newlines in the subtitle will be replaced with carets (^) and a
        //  backslash (\) will be added to both the beginning and end.
        /// </summary>
        public static byte[] GetSubtitleBytes(string input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Match match = Regex.Match(input, @"[\\^]");
            if (match.Success)
            {
                throw new ArgumentException("Use of \\ or ^ is illegal. Value: " + input, nameof(input));
            }
            string sanitized = input.Replace(Environment.NewLine, "^");
            return Encoding.UTF8.GetBytes($"\\{sanitized}\\" + Environment.NewLine);
        }
    }

    /// <summary>
    /// A list of predefined languages used in the original TDPACK file.
    /// </summary>
    public enum Language
    {
        Japanese,
        English,
        German,
        French,
        Spanish,
        Italian,
        UsFrench,
        UsSpanish
    }
}